package com.example.artyom.fifteen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.artyom.fifteen.MainApplication.Companion.SIZE
import com.example.artyom.fifteen.MainApplication.Companion.X_SIZE
import com.example.artyom.fifteen.MainApplication.Companion.field
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.CountDownLatch

class MainActivity : AppCompatActivity() {

    internal val context = this

    internal lateinit var mainApp: MainApplication

    internal val ACTIVITY_TAG = "Activity"

    internal val SWIPE_TAG = "Swipe"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(ACTIVITY_TAG, "create")
        setContentView(R.layout.activity_main)

        mainApp = this.application as MainApplication

        mainGrid.adapter = GridAdapter(this, R.layout.list_item)

        mainGrid.numColumns = X_SIZE
        mainGrid.setOnTouchListener(TouchListener(this, this))

        refreshButton.setOnClickListener {
            mainApp.generateField()
            mainGrid.invalidateViews()
        }

        MobileAds.initialize(applicationContext, sideAdUnitId)

        val adRequest = AdRequest.Builder().build()
        sideAd1!!.loadAd(adRequest)
        sideAd2!!.loadAd(adRequest)
        sideAd3!!.loadAd(adRequest)
    }

    internal fun checkIfHasWon() {
        val hasWon = (0..SIZE - 2).all { field[it / X_SIZE][it % X_SIZE] == it + 1 }

        if (hasWon) {
            Thread(Runnable {
                doneSignal = CountDownLatch(1)
                val intent = Intent(context, WinActivity::class.java)
                this@MainActivity.startActivity(intent)
                doneSignal.await()

                runOnUiThread {
                    mainApp.generateField()
                    mainGrid.invalidateViews()
                }
            }).start()
        }
    }

    internal class GridAdapter(context: Context, var resource: Int) : BaseAdapter() {
        val inflater = LayoutInflater.from(context)!!

        override fun getCount() = SIZE

        override fun getItem(i: Int) = null

        override fun getItemId(i: Int): Long = 0

        override fun getView(position: Int, view: View?, viewGroup: ViewGroup): View {
            val result = (view ?: this.inflater.inflate(this.resource, viewGroup, false)) as TextView

            val number = field[position / X_SIZE][position % X_SIZE]
            val text = if (number == 0) "" else number.toString()

            result.text = text
            result.textSize = MainActivity.TEXT_SIZE.toFloat()
            result.textAlignment = View.TEXT_ALIGNMENT_CENTER

            return result
        }
    }

    companion object {

        private val sideAdUnitId = "ca-app-pub-2284002861557744/9262195711"

        internal val TEXT_SIZE = 45

        internal lateinit var doneSignal: CountDownLatch
    }
}
