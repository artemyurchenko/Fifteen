package com.example.artyom.fifteen

import android.app.Activity
import android.os.Bundle
import android.os.SystemClock
import com.example.artyom.fifteen.MainActivity.Companion.doneSignal
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.activity_win.*

class WinActivity : Activity() {
    private val delay = 5

    internal lateinit var mInterstitialAd: InterstitialAd

    fun getCongratulationText(time: Int) = "You have won! Prepare to watch an ad in " + time + "s."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_win)

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = "ca-app-pub-2284002861557744/7585156112"
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() = requestNewInterstitial()
        }
        requestNewInterstitial()

        var time = delay

        object : Thread() {
            override fun run() {
                while (!isInterrupted) {
                    SystemClock.sleep(1000)
                    runOnUiThread {
                        congratulations.text = getCongratulationText(time--)
                        if (time == 0) {
                            mInterstitialAd.show()
                            interrupt()
                            finish()
                        }
                    }
                }
                doneSignal.countDown()
            }
        }.start()

    }

    private fun requestNewInterstitial() = mInterstitialAd.loadAd(AdRequest.Builder().build())
}
